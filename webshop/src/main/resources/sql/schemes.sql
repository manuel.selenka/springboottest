CREATE TABLE IF NOT EXISTS userProfil (
    u_id INT AUTO_INCREMENT PRIMARY KEY,
    firstname VARCHAR(60),
    lastname VARCHAR(60),
    email VARCHAR(60),
    adress VARCHAR(60),
    postcode VARCHAR(60),
    country VARCHAR(60),
    credit FLOAT
);

CREATE TABLE IF NOT EXISTS products (
    p_id NUMBER AUTO_INCREMENT PRIMARY KEY,
    product_name VARCHAR(60) NOT NULL,
    price_per_unit FLOAT NOT NULL,
    stock UNSIGNED INT NOT NULL,
    description VARCHAR(60),
    seller INT NOT NULL,
    FOREIGN KEY (seller) REFERENCES userProfile(u_id)
);

CREATE TABLE IF NOT EXISTS shopping_cart (
    sc_id INT AUTO_INCREMENT PRIMARY KEY,
    total_price FLOAT NOT NULL,
    shopping_card BOOLEAN NOT NULL
);

CREATE TABLE IF NOT EXISTS roleUser (
    u_id INT NOT NULL,
    user_role ENUM ('buyer', 'seller') NOT NULL,
    PRIMARY KEY (u_id, user_role),
    FOREIGN KEY (u_id) REFERENCES userProfile(u_id)
);

CREATE TABLE IF NOT EXISTS orderList (
    o_id INT AUTO_INCREMENT NOT NULL,
    u_id INT NOT NULL,
    p_id INT NOT NULL,
    PRIMARY KEY (o_id, u_id, p_id),
    FOREIGN KEY (u_id) REFERENCES userProfile(u_id),
    FOREIGN KEY (p_id) REFERENCES product(p_id)
);

CREATE TABLE IF NOT EXISTS containedIn (
    p_id INT NOT NULL,
    sc_id INT NOT NULL,
    selected UNSIGNED INT NOT NULL,
    PRIMARY KEY (p_id, sc_id),
    FOREIGN KEY (p_id) REFERENCES product(p_id),
    FOREIGN KEY (sc_id) REFERENCES shopping_cart(sc_id)
);

CREATE TABLE IF NOT EXISTS paid_cart (
    pd_id INT AUTO_INCREMENT NOT NULL,
    sc_id INT NOT NULL,
    u_id INT NOT NULL,
    booking_day DATE NOT NULL,
    shipping_day DATE NOT NULL,
    payment_method ENUM('SEPA','cash','PayPal','Credit Card') NOT NULL,
    PRIMARY KEY (pd_id, u_id),
    FOREIGN KEY (sc_id) REFERENCES shopping_cart(sc_id),
    FOREIGN KEY (u_id) REFERENCES userProfile(u_id)
);
