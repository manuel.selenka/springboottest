package com.testproduct.webshop.repository

import com.testproduct.webshop.entity.Contained_in
import com.testproduct.webshop.entity.Contained_in_Id
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository


@Repository
interface Contained_inRepo : JpaRepository<Contained_in, Contained_in_Id> {
}
