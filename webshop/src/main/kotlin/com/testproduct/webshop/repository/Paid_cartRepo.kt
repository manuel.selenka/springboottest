package com.testproduct.webshop.repository

import com.testproduct.webshop.entity.Paid_cart
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface Paid_cartRepo : JpaRepository<Paid_cart, Long> {
}
