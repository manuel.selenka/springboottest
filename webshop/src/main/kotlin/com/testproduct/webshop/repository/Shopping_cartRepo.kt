package com.testproduct.webshop.repository

import com.testproduct.webshop.entity.Shopping_cart
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface Shopping_cartRepo : JpaRepository<Shopping_cart, Long> {
}
