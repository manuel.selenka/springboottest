package com.testproduct.webshop.repository

import com.testproduct.webshop.entity.Role
import com.testproduct.webshop.entity.Role_id
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface RoleRepo : JpaRepository<Role, Role_id> {
}
