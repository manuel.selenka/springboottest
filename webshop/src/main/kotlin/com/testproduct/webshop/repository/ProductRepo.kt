package com.testproduct.webshop.repository

import com.testproduct.webshop.Product
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ProductRepo : JpaRepository<Product, Long> {
}
