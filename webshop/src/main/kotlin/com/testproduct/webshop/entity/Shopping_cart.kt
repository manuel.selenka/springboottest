package com.testproduct.webshop.entity

import javax.persistence.*

@Entity
@Table (name = "shopping_cart")
data class Shopping_cart (

    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    @Column(name = "shopping_cart")
    val sc_id : Long,

    @Column(name = "price_total")
    val price_total : Float,

    @Column(name = "shopping_card")
    val shopping_cart : Boolean,

    @ManyToMany
    @JoinColumn(name = "sc_id", referencedColumnName = "User.u_id")
    val owner_id : Set<User>,


    /*@ManyToMany
    @JoinColumn(name = "sc_id", referencedColumnName = "Contained_in.shopping_cart_list")
    val contained_In_list: Set<Contained_in>*/
)

