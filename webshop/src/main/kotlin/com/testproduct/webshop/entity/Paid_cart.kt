package com.testproduct.webshop.entity

import javax.persistence.*

@Entity
@Table (name = "Paid_cart")
data class Paid_cart (

    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    @Column (name = "pc_id")
    val pc_id : Long,

    @Column (name = "booking_day")
    val bookin_day : String,

    @Column (name = "shipping_day")
    val shipping_day : String,

    @Column (name = "payment_method")
    val payment_method : Payment_method_enum,

    @ManyToMany
    @JoinColumn (referencedColumnName = "User.u_id", name = "pc_id")
    val buyer : Set<User>
)
