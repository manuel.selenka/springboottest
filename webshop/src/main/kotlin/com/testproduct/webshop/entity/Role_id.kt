package com.testproduct.webshop.entity

//import kotlinx.serialization.Serializable
import javax.persistence.Embeddable
import javax.persistence.EnumType
import javax.persistence.Enumerated
import java.io.Serializable

data class Role_id (
    val u_id : Long,
    val role_name : Role_enum
): Serializable
