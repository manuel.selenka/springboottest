package com.testproduct.webshop

import com.testproduct.webshop.entity.Contained_in
//import com.testproduct.webshop.entity.Order
import com.testproduct.webshop.entity.User
import javax.persistence.*
import kotlin.collections.MutableSet

@Entity
@Table(name = "Product")
data class Product(
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    @Column(name="p_id", nullable = false)
    val p_id : Long,

    @Column(name="name") val name: String,

    @Column(name="price_per_unit") val price_per_unit : Float,

    @Column(name="stock") val stock : UInt,

    @Column(name="description") val description : String,

    @ManyToOne (fetch=FetchType.LAZY)
    @JoinColumn (name = "u_id")
    val seller : User,
/*
    @ManyToMany
    @JoinColumn (referencedColumnName = "Order.o-id", name = "p_id")
    val order_list : Set<Order>,

    @ManyToMany
    @JoinColumn (name = "p_id", referencedColumnName = "Contained_in.product_list", nullable = false)
    val contained_in_list : Set<Contained_in>*/
)




