package com.testproduct.webshop.entity

//import kotlinx.serialization.Serializable
import javax.persistence.*
import java.io.Serializable

@Entity
@Table(name = "Role")
@IdClass(Role_id::class)
data class Role(
    @Id

    @Enumerated (EnumType.STRING)
    @Column(name = "Role_name")
    val role_name: Role_enum,


    @ManyToOne (fetch=FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    val u_id: User,

): Serializable

