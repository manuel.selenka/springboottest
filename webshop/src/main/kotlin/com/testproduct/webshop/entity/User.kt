package com.testproduct.webshop.entity

import com.testproduct.webshop.Product
import javax.persistence.*


@Entity
@Table(name = "User")
data class User(
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    @Column(name = "u_id")
    val u_id: Long,

    @Column(name="first_name")
    val first_name: String,

    @Column(name="last_name")
    val last_name: String,

    @Column(name="email")
    val email: String,

    @Column(name="adress")
    val adress: String,

    @Column(name="postcode")
    val postcode: String,

    @Column(name="country")
    val country: String,

    @Column(name="credit")
    val credit: Double,

    @OneToMany(mappedBy = "u_id")
    val role: Set<Role>,

    @OneToMany(mappedBy = "seller")
    val selling_product: Set<Product>,

    @ManyToMany
    @JoinColumn(referencedColumnName = "shopping_carts", name = "u_id")
    val shopping_carts: Set<Shopping_cart>,

/*

    @ManyToMany
    @JoinColumn (name = "o_id", referencedColumnName = "Order.o_id")
    val order_list: Set<Order>,
*/
    @ManyToMany
    @JoinColumn (referencedColumnName = "Paid_cart.pc_id", name = "u_id")
    val paid_cart_list: Set<Paid_cart>
)
