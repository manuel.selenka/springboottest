package com.testproduct.webshop.entity

import com.testproduct.webshop.Product
import java.io.Serializable

data class Contained_in_Id (
    val shopping_cart_list : Long,
    val product_list : Long
) : Serializable
