package com.testproduct.webshop.entity

import com.testproduct.webshop.Product
import java.io.Serializable
import javax.persistence.*
//import kotlinx.serialization.Serializable

@Entity
@Table(name = "Contained_in")
@IdClass(Contained_in_Id::class)
data class Contained_in (
    @Id
    //@Serializable
    //@EmbeddedId

    @Column (name = "shopping_cart_list")
    val shopping_cart_list : Long,

    @Column (name = "selected_number")
    val selected_number : Int,


    val product_list : Long
    /*@ManyToMany
    @JoinColumn(name = "shopping_cart_list", referencedColumnName = "shopping_cart.contained_In_list")
    val shopping_cart_list : Set<Shopping_cart>,


    @ManyToMany
    @JoinColumn (name = "Contained_in", referencedColumnName = "Product.p_id", nullable = false)
    val product_list : Set<Product>*/

) : Serializable
