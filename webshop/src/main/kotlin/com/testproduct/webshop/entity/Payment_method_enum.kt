package com.testproduct.webshop.entity

enum class Payment_method_enum {
    PayPal,
    Credit_Card,
    Cash,
    SEPA
}
